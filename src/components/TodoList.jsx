import TodoItem from './TodoItem';

function TodoList({ list, onCheckedChanged, isDeleteEnabled, onDeleteItem }) {
  return (
    <div>
      {list.map(({ isCompleted, details, id }) => (
        <TodoItem
          key={id}
          isCompleted={isCompleted}
          details={details}
          id={id}
          onCheckedChanged={onCheckedChanged}
          isDeleteEnabled={isDeleteEnabled}
          onDeleteItem={onDeleteItem}
        />
      ))}
    </div>
  );
}

TodoList.defaultProps = {
  list: [],
  isDeleteEnabled: false,
  onCheckedChanged: () => {},
  onDeleteItem: () => {},
};

export default TodoList;
