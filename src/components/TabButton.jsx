import './TabButton.styles.css';

function TabButton({ index, currentIndex, label, onTabIndexChanged }) {
  const handleTabIndexChanged = () => {
    onTabIndexChanged(index);
  };

  const className =
    index === currentIndex ? 'tab-button tab-button--active' : 'tab-button';

  return (
    <button className={className} onClick={handleTabIndexChanged}>
      {label}
    </button>
  );
}

TabButton.defaultProps = {
  index: -1,
  currentIndex: 0,
  label: '',
  onTabIndexChanged: () => {},
};

export default TabButton;
