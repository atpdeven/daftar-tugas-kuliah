import { useEffect, useState } from "react";

// helpers
import { getLocalStorageItem, saveToLocalStorage } from "../util/localstorage";
import { INTIAL_TAB_INDEX, TABS } from "../configs/tabs";

// components
import TodoInput from "./TodoInput";
import TodoList from "./TodoList";
import Tabs from "./Tabs";
import TabView from "./TabView";
import Footer from "./footer";

// styles
import "./App.styles.css";
import { createCurrentTimestamp } from "../util/time";

function App() {
  const initialTodo = getLocalStorageItem();
  const [todoList, setTodo] = useState(initialTodo);
  const [currentTabIndex, setTabIndex] = useState(INTIAL_TAB_INDEX);

  // Watch todo list state to sync with localstorage
  useEffect(() => {
    saveToLocalStorage(todoList);
  }, [todoList]);

  // CRUD Handler for todo states
  const handleCheckedChanged = (id, isCompleted) => {
    const mutated = todoList.map((todoItem) => {
      if (todoItem.id === id) {
        return { ...todoItem, isCompleted };
      } else {
        return todoItem;
      }
    });
    setTodo(mutated);
  };

  const handleAddTodoItem = (details) => {
    const updated = [
      ...todoList,
      {
        // identifier for todo items
        id: createCurrentTimestamp(),
        isCompleted: false,
        details,
      },
    ];
    setTodo(updated);
  };

  const handleDeleteAll = () => {
    const afterDelete = todoList.filter((item) => !item.isCompleted);
    setTodo(afterDelete);
  };

  const handleDeleteItem = (id) => {
    const afterDeleted = todoList.filter((item) => item.id !== id);
    setTodo(afterDeleted);
  };

  // Tab Handlers
  const handleTabIndexChanged = (tabIndex) => {
    setTabIndex(tabIndex);
  };

  // Arrange displayed data
  const { isDeleteable } = TABS[currentTabIndex];
  const completedTodos = todoList.filter(({ isCompleted }) => isCompleted);
  const activeTodos = todoList.filter(({ isCompleted }) => !isCompleted);

  // render app
  return (
    <div className="App">
      <h1 className="title">Daftar Tugas Kuliah</h1>
      <Tabs tabs={TABS} onTabIndexChanged={handleTabIndexChanged} currentIndex={currentTabIndex} />
      <TodoInput onAddTodoItem={handleAddTodoItem} />
      <TabView currentIndex={currentTabIndex} tabIndex={0}>
        <TodoList list={todoList} onCheckedChanged={handleCheckedChanged} />
      </TabView>
      <TabView currentIndex={currentTabIndex} tabIndex={1}>
        <TodoList list={activeTodos} onCheckedChanged={handleCheckedChanged} />
      </TabView>
      <TabView currentIndex={currentTabIndex} tabIndex={2}>
        <TodoList list={completedTodos} onCheckedChanged={handleCheckedChanged} onDeleteItem={handleDeleteItem} isDeleteEnabled />
      </TabView>
      {isDeleteable && (
        <button className="delete-button" onClick={handleDeleteAll}>
          delete all
        </button>
      )}

      <Footer />
    </div>
  );
}

export default App;
