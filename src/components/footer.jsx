import React from "react";
import { Box, Container, Row, Column, FooterLink, Heading } from "./FooterStyles";

const Footer = () => {
  return (
    <Box>
      <Container>
        <Row>
          <Column>
            <Heading>Social Media</Heading>
            <FooterLink href="https://www.facebook.com/AbdiJepriBangun/" target="_blank">
              <i className="fab fa-facebook-f">
                <span style={{ marginLeft: "10px" }}>Facebook</span>
              </i>
            </FooterLink>
            <FooterLink href="http://instagram.com/abdijepri/" target="_blank">
              <i className="fab fa-instagram">
                <span style={{ marginLeft: "10px" }}>Instagram</span>
              </i>
            </FooterLink>
            <FooterLink href="http://twitter.com/AbdiJepri" target="_blank">
              <i className="fab fa-twitter">
                <span style={{ marginLeft: "10px" }}>Twitter</span>
              </i>
            </FooterLink>
            <FooterLink href="http://discord.com/app" target="_blank">
              <i className="fab fa-youtube">
                <span style={{ marginLeft: "10px" }}>Discord</span>
              </i>
            </FooterLink>
          </Column>
          <Column>
            <Heading>Check Tugas</Heading>
            <FooterLink href="https://lms.telkomuniversity.ac.id/" target="_blank">
              <i className="fab fa-facebook-f">
                <span style={{ marginLeft: "10px" }}>LMS Telkom</span>
              </i>
            </FooterLink>
          </Column>
        </Row>
      </Container>
    </Box>
  );
};
export default Footer;
