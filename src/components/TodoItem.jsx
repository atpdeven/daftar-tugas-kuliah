import "./TodoItem.styles.css";
import { FaTrashRestoreAlt } from "react-icons/fa";

function TodoItem({ isDeleteEnabled, isCompleted, details, onCheckedChanged, onDeleteItem, id }) {
  const key = `todo-item-${id}`;

  const detailsClassname = isCompleted ? "todo-item-details todo-item-details--completed" : "todo-item-details";

  const handleCheckedChanged = (element) => {
    const value = element.target.checked;
    onCheckedChanged(id, value);
  };

  const handleDeleteClick = () => {
    onDeleteItem(id);
  };

  return (
    <div className="todo-item-container">
      <label htmlFor={key} className="todo-item-container-left">
        <input className="todo-item-checkbox" type="checkbox" name={key} id={key} checked={isCompleted} onChange={handleCheckedChanged} />
        <span className={detailsClassname}>{details}</span>
      </label>
      {isDeleteEnabled && <FaTrashRestoreAlt className="delete-button" onClick={handleDeleteClick} />}
    </div>
  );
}

TodoItem.defaultProps = {
  isCompleted: false,
  details: "",
  onCheckedClick: () => {},
  isDeleteEnabled: false,
  onDeleteItem: () => {},
};

export default TodoItem;
