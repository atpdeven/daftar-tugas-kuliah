function TabView({ tabIndex, currentIndex, children }) {
  if (currentIndex !== tabIndex) {
    return null;
  }

  return <>{children}</>;
}

TabView.defaultProps = {};

export default TabView;
