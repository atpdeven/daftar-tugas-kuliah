import React, { useState } from "react";

const INITIAL_TODO = [
  {
    details: "contoh todo Item 1",
    isCompleted: false,
  },
  {
    details: "contoh todo Item 2",
    isCompleted: true,
  },
];

const TABS = [
  {
    label: "All",
    isDeleteable: false,
  },
  {
    label: "Active",
    isDeleteable: false,
  },
  {
    label: "Completed",
    isDeleteable: true,
  },
];

const INITIAL_TAB_INDEX = 0;

function HomePage() {
  const [todoList, setTodo] = useState(INITIAL_TODO);
  const [details, setDetails] = useState("");
  const [currentTabIndex, setTabIndex] = useState(INITIAL_TAB_INDEX);

  const onHandleClick = (index) => (e) => {
    const value = e.target.checked;
    const mutated = todoList.map((todoItem, currentIndex) => {
      if (currentIndex === index) {
        return { ...todoItem, isCompleted: value };
      } else {
        return todoItem;
      }
    });
    setTodo(mutated);
  };

  const handleAddTodo = () => {
    setTodo([...todoList, { details, isCompleted: false }]);
    setDetails("");
  };

  const handleToDoInputChange = (e) => {
    const value = e.target.value;
    setDetails(value);
  };

  const handleTabIndexChanged = (tabIndex) => () => {
    setTabIndex(tabIndex);
  };
  const { isDeleteable } = TABS[currentTabIndex];

  const completedTodos = todoList.filter(({ isCompleted }) => isCompleted);
  const activeTodos = todoList.filter(({ isCompleted }) => !isCompleted);

  return (
    <div className="container">
      {TABS.map(({ label }, index) => (
        <button onClick={handleTabIndexChanged(index)}>{label}</button>
      ))}

      <input name="add-todo" id="add-todo" onChange={handleToDoInputChange}></input>
      <button onClick={handleAddTodo}>Add</button>
      <br />

      {currentTabIndex === 1}

      {todoList.map(({ details, isCompleted }, index) => (
        <label>
          <input type="checkbox" name="todo-checkbox" checked={isCompleted} onChange={onHandleClick(index)} value={details}></input>
          <span>{details}</span>
        </label>
      ))}
      {isDeleteable && <button>Delete</button>}
    </div>
  );
}

export default HomePage;
