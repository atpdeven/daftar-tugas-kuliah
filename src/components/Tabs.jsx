import TabButton from './TabButton';
import './Tabs.styles.css';

function Tabs({ tabs, onTabIndexChanged, currentIndex }) {
  const handleTabIndexChanged = (index) => {
    onTabIndexChanged(index);
  };

  return (
    <div className="tabs">
      {tabs.map(({ label }, index) => (
        <TabButton
          label={label}
          index={index}
          onTabIndexChanged={handleTabIndexChanged}
          currentIndex={currentIndex}
        />
      ))}
    </div>
  );
}

Tabs.defaultProps = {
  tabs: [],
  currentIndex: 0,
};

export default Tabs;
