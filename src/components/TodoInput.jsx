import { useState } from "react";
import "./TodoInput.styles.css";

function TodoInput({ onAddTodoItem }) {
  const [details, setDetails] = useState("");

  const handleTodoInputChanged = (e) => {
    const value = e.target.value;
    setDetails(value);
  };

  const handleTodoItemClick = () => {
    if (details.length <= 0) {
      return;
    }

    onAddTodoItem(details);
    setDetails("");
  };

  return (
    <div className="todo-input-container">
      <input
        name="add-todo"
        id="add-todo"
        placeholder="Tambah Tugas - Deadline"
        onChange={handleTodoInputChanged}
        value={details}
        className="todo-input-textinput"
        onKeyPress={(e) => {
          if (e.key === "Enter") {
            handleTodoItemClick();
          }
        }}
      />
      <button onClick={handleTodoItemClick} className="todo-input-button">
        Add
      </button>
    </div>
  );
}

TodoInput.defaultProps = {
  onAddTodoItem: () => {},
};

export default TodoInput;
