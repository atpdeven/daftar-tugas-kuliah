export function saveToLocalStorage(state = []) {
  if (state.length < 0) {
    return;
  }

  if (!window) {
    return;
  }

  const jsonString = JSON.stringify(state); 

  window.localStorage.setItem('todo-state', jsonString);
}

export function getLocalStorageItem() {
  if (!window) {
    return [];
  }
  
  const jsonString = window.localStorage.getItem('todo-state');
  if (!jsonString) {
    return [];
  }

  const json = JSON.parse(jsonString);
  if (!json) {
    return [];
  }

  return json;
}