export function createCurrentTimestamp() {
  return Math.round(new Date().getTime()/1000);
};