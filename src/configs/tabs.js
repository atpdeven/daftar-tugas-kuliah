export const TABS = [
  { label: 'All', isDeleteable: false },
  { label: 'Active', isDeleteable: false },
  { label: 'Completed', isDeleteable: true },
];

export const INTIAL_TAB_INDEX = 0;
